require 'simplecov'
SimpleCov.start  do
  add_filter 'spec'
  coverage_dir 'tmp/coverage'
end

require_relative '../lib/rtree'
require 'fileutils'

Dir[File.dirname(__FILE__) + '/support/*/*.rb'].each do |path|
  require path
end

def with_a_silent(stream)
  old_stream = stream.dup
  stream.reopen('/dev/null')
  stream.sync = true
  yield
ensure
  stream.reopen(old_stream)
end

def with_temporary_file(name)
  path = File.join('/tmp', name)
  begin
    yield path
  ensure
    FileUtils.rm_f path
  end
end

def fixture(file)
  File.join(File.dirname(__FILE__), 'fixtures', file)
end

RSpec.configure do |c|
  c.alias_example_to :they
end
