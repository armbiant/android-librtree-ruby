describe 'RTree' do
  describe '#branch_size' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :branch_size
    end

    subject { rtree.branch_size }

    it 'does not raise' do
      expect { subject.to_not raise_error }
    end

    it 'is an Integer' do
      expect(subject).to be_an Integer
    end

    it 'agrees with #to_h' do
      expect(subject).to eq rtree.to_h[:state][:branch_size]
    end
  end
end
