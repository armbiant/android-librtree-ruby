describe 'RTree' do
  describe '#add_rect' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :add_rect
    end

    subject { rtree.add_rect(id, coords) }

    context 'non-integer first argument' do
      let(:id) { '43' }
      let(:coords) { [0, 0, 1, 1] }

      it 'raises TypeError' do
        expect { subject }.to raise_error TypeError
      end
    end

    context 'non-array second argument' do
      let(:id) { 43 }
      let(:coords) { 'array' }

      it 'raises TypeError' do
        expect { subject }.to raise_error TypeError
      end
    end

    context 'correct sized array' do
      let(:id) { 43 }
      let(:coords) { [0, 0, 1, 1] }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'returns self' do
        expect(subject).to eq rtree
      end
    end

    context 'array of non-numbers' do
      let(:id) { 43 }
      let(:coords) { [0, 0, '1', 1] }

      it 'raises TypeError' do
        expect { subject }.to raise_error TypeError
      end
    end

    context 'array too short' do
      let(:id) { 43 }
      let(:coords) { [0, 0, 1] }

      it 'raises ArgumentError' do
        expect { subject }.to raise_error ArgumentError
      end
    end

    context 'array too long' do
      let(:id) { 43 }
      let(:coords) { [0, 0, 1, 1, 2] }

      it 'raises ArgumentError' do
        expect { subject }.to raise_error ArgumentError
      end
    end

    context 'array inconsistent' do
      let(:id) { 43 }
      let(:coords) { [1, 1, 0, 0] }

      it 'raises Errno::EINVAL' do
        expect { subject }.to raise_error Errno::EINVAL
      end
    end

    context 'array degenerate' do
      let(:id) { 43 }
      let(:coords) { [0, 0, 0, 1] }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'returns self' do
        expect(subject).to eq rtree
      end
    end
  end
end
