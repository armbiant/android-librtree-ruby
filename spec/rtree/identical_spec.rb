describe 'RTree' do
  describe '==' do

    it 'responds' do
      rtree = RTree.new(2)
      expect(rtree).to respond_to :==
    end

    describe 'identical objects' do
      let(:rtree_A) { RTree.new(2) }
      let(:rtree_B) { rtree_A }

      they 'are ==' do
        expect(rtree_A == rtree_B).to eq true
      end
    end

    describe 'identical trees' do
      let(:rtree_A) { RTree.new(2) }
      let(:rtree_B) { RTree.new(2) }

      context 'empty' do

        they 'are ==' do
          expect(rtree_A == rtree_B).to eq true
        end
      end

      context 'non-empty' do
        before do
          [rtree_A, rtree_B].each do |rtree|
            rtree.add_rect(1, [0, 0, 1, 1])
          end
        end

        they 'are ==' do
          expect(rtree_A == rtree_B).to eq true
        end
      end
    end

    describe 'different dimensions' do
      let(:rtree_A) { RTree.new(2) }
      let(:rtree_B) { RTree.new(3) }

      they 'are not ==' do
        expect(rtree_A == rtree_B).to eq false
      end
    end

    describe 'different ids' do
      let(:rtree_A) { RTree.new(2) }
      let(:rtree_B) { RTree.new(2) }

      before do
        rtree_A.add_rect(1, [0, 0, 1, 1])
        rtree_B.add_rect(2, [0, 0, 1, 1])
      end

      they 'are not ==' do
        expect(rtree_A == rtree_B).to eq false
      end
    end

    describe 'different rectangles' do
      let(:rtree_A) { RTree.new(2) }
      let(:rtree_B) { RTree.new(2) }

      before do
        rtree_A.add_rect(1, [0, 0, 1, 1])
        rtree_B.add_rect(1, [0, 0, 1, 2])
      end

      they 'are not ==' do
        expect(rtree_A == rtree_B).to eq false
      end
    end
  end
end
