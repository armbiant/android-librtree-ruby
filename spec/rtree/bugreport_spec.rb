describe 'RTree' do
  describe '.bugreport' do

    it 'responds' do
      expect(RTree).to respond_to :bugreport
    end

    subject { RTree.bugreport }

    it 'does not raise' do
      expect { subject }.to_not raise_error
    end

    it 'is a string' do
      expect(subject).to be_a String
    end

    it 'is the expected string' do
      expect(subject).to eq 'j.j.green@gmx.co.uk'
    end
  end
end
