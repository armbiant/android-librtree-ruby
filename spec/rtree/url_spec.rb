describe 'RTree' do
  describe '.url' do

    it 'responds' do
      expect(RTree).to respond_to :url
    end

    subject { RTree.url }

    it 'does not raise' do
      expect { subject }.to_not raise_error
    end

    it 'is a string' do
      expect(subject).to be_a String
    end

    it 'is the expected string' do
      expect(subject).to eq 'https://gitlab.com/jjg/librtree'
    end
  end
end
