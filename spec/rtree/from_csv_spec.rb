describe 'RTree' do
  describe '.from_csv' do

    it 'responds' do
      expect(RTree).to respond_to :from_csv
    end

    subject { RTree.from_csv(csv, 2) }

    describe 'nil' do
      let(:csv) { nil }

      it 'raises' do
        expect { subject }.to raise_error TypeError
      end
    end

    describe 'hash' do
      let(:csv) { { key: 'value' } }

      it 'raises' do
        expect { subject }.to raise_error TypeError
      end
    end

    describe 'invalid CSV (a line too short)' do
      let(:csv) do
        <<~EOF
          1, 0, 0, 1, 1
          2, 1, 1, 2
        EOF
      end

      it 'raises' do
        expect { subject }.to raise_error RuntimeError
      end
    end

    describe 'valid CSV' do
      let(:csv) do
        <<~EOF
          1, 0, 0, 1, 1
          2, 1, 1, 2, 2
        EOF
      end

      it 'raises' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree' do
        expect(subject).to be_an RTree
      end

      it 'is non-empty' do
        expect(subject).to_not be_empty
      end
    end
  end
end
