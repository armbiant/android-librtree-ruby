describe 'RTree' do
  describe 'SPLIT_QUADRATIC' do

    it 'is defined' do
      expect(RTree).to have_constant :SPLIT_QUADRATIC
    end

    subject { RTree::SPLIT_QUADRATIC }

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'has the expected value' do
      expect(subject).to eq 0
    end
  end

  describe 'SPLIT_LINEAR' do

    it 'is defined' do
      expect(RTree).to have_constant :SPLIT_LINEAR
    end

    subject { RTree::SPLIT_LINEAR }

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'has the expected value' do
      expect(subject).to eq 1
    end
  end

  describe 'SPLIT_GREENE' do

    it 'is defined' do
      expect(RTree).to have_constant :SPLIT_GREENE
    end

    subject { RTree::SPLIT_GREENE }

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'has the expected value' do
      expect(subject).to eq 2
    end
  end

  describe 'AXIS_WIDTH' do

    it 'is defined' do
      expect(RTree).to have_constant :AXIS_WIDTH
    end

    subject { RTree::AXIS_WIDTH }

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'has the expected value' do
      expect(subject).to eq 0
    end
  end

  describe 'AXIS_HEIGHT' do

    it 'is defined' do
      expect(RTree).to have_constant :AXIS_HEIGHT
    end

    subject { RTree::AXIS_HEIGHT }

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'has the expected value' do
      expect(subject).to eq 1
    end
  end
end
