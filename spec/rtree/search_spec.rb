describe 'RTree' do
  describe '#search' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :search
    end

    context 'with a block' do

      context 'empty' do
        let(:rect) { [0, 0, 1, 1] }

        it 'does not yield' do
          expect { |b| rtree.search(rect, &b) }
            .to_not yield_control
        end
      end

      context 'single rectangle' do
        let(:id) { 23 }
        let(:rect) { [0, 0, 1, 1] }

        before { rtree.add_rect(id, rect) }

        it 'yields' do
          expect { |b| rtree.search(rect, &b) }
            .to yield_control
        end

        it 'yields the expected id' do
          expect { |b| rtree.search(rect, &b) }
            .to yield_with_args(23)
        end
      end

      context 'multiple rectangles' do
        before do
          [
            [21, [0, 0, 1, 1]],
            [22, [1, 1, 2, 2]],
            [23, [2, 2, 3, 3]]
          ].each { |(id, rect)| rtree.add_rect(id, rect) }
        end

        let(:matched_ids) do
          ids = []
          rtree.search(rect) { |id| ids << id }
          ids
        end

        context 'search rectangle matches all' do
          let(:rect) { [0, 0, 3, 3] }

          it 'yields the expected number of times' do
            expect { |b| rtree.search(rect, &b) }
              .to yield_control.exactly(3).times
          end

          it 'matches the expected ids' do
            expect(matched_ids).to match_array [21, 22, 23]
          end
        end

        context 'search rectangle matches some' do
          let(:rect) { [1.5, 1.5, 3, 3] }

          it 'yields the expected number of times' do
            expect { |b| rtree.search(rect, &b) }
              .to yield_control.exactly(2).times
          end

          it 'matches the expected ids' do
            expect(matched_ids).to match_array [22, 23]
          end
        end

        context 'search rectangle matches none' do
          let(:rect) { [4, 4, 5, 5] }

          it 'yields the expected number of times' do
            expect { |b| rtree.search(rect, &b) }
              .to_not yield_control
          end

          it 'matches the expected ids' do
            expect(matched_ids).to be_empty
          end
        end

        context 'interaction with break' do
          let(:rect) { [0, 0, 3, 3] }

          it 'breaks the iteration over matches' do
            ids = []
            rtree.search(rect) do |id|
              ids << id
              break
            end
            expect(ids.length).to eq 1
            expect([21, 22, 23]).to include(ids.first)
          end
        end

        context 'interaction with raise' do
          let(:rect) { [0, 0, 3, 3] }

          it 'breaks the iteration over matches' do
            ids = []
            begin
              rtree.search(rect) do |id|
                ids << id
                raise RuntimeError, 'meh'
              end
            rescue RuntimeError
            end
            expect(ids.length).to eq 1
            expect([21, 22, 23]).to include(ids.first)
          end
        end
      end
    end

    context 'without a block' do
      subject { rtree.search(rect) }

      context 'empty' do
        let(:rect) { [0, 0, 1, 1] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is empty' do
          expect(subject).to be_empty
        end
      end

      context 'single rectangle' do
        let(:id) { 23 }
        let(:rect) { [0, 0, 1, 1] }

        before { rtree.add_rect(id, rect) }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is the expected array' do
          expect(subject).to match_array [id]
        end
      end

      context 'multiple rectangles' do
        before do
          [
            [21, [0, 0, 1, 1]],
            [22, [1, 1, 2, 2]],
            [23, [2, 2, 3, 3]]
          ].each { |(id, rect)| rtree.add_rect(id, rect) }
        end

        context 'search rectangle matches all' do
          let(:rect) { [0, 0, 3, 3] }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          it 'is the expected array' do
            expect(subject).to match_array [21, 22, 23]
          end
        end

        context 'search rectangle matches some' do
          let(:rect) { [1.5, 1.5, 3, 3] }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          it 'is the expected array' do
            expect(subject).to match_array [22, 23]
          end
        end

        context 'search rectangle matches none' do
          let(:rect) { [4, 4, 5, 5] }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          it 'is the expected array' do
            expect(subject).to be_empty
          end
        end
      end
    end

    describe 'the Guttman-Green test' do

      # this test-case is included in the original Guttman-Green
      # implementation

      before do
        [
          [1, [0, 0, 2, 2]],
          [2, [5, 5, 7, 7]],
          [3, [8, 5, 9, 6]],
          [4, [7, 1, 9, 2]]
        ].each { |args| rtree.add_rect(*args) }
      end

      subject { rtree.search(target) }
      let(:target) { [6, 4, 10, 6] }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'finds the expected indices' do
        expect(subject).to match_array [2, 3]
      end
    end

    describe 'regular grid test' do

      # When the input rectangles are on a regular grid, then
      # one can easily work out which of those rectangles would
      # intersect a given search rectangle.  This is taken from
      # the librtree test suite (but is rather more compact)

      before do
        (0...n).each do |i|
          (0...n).each do |j|
            coord = [i, j, i + 1, j + 1]
            id = i + j * n
            rtree.add_rect(id, coord)
          end
        end
      end

      def grid(n, coord)
        xmin = [coord[0], 0].max.floor
        xmax = [coord[2], n - 1].min.floor
        ymin = [coord[1], 0].max.floor
        ymax = [coord[3], n - 1].min.floor
        ids = []
        (xmin..xmax).each do |i|
          (ymin..ymax).each do |j|
            ids << i + j * n
          end
        end
        ids
      end

      context 'small grid' do

        # this is to give is confidence that grid() is producing
        # the right thing

        let(:n) { 10 }
        let(:coord) { [1.25, 0.5, 1.75, 1.5] }
        let(:ids) { [1, 11] }

        describe 'the grid calculation' do

          it 'finds the expected ids' do
            expect(grid(n, coord)).to match_array ids
          end
        end

        describe 'rtree search' do

          it 'finds the expected ids' do
            expect(rtree.search(coord)).to match_array ids
          end
        end
      end

      context 'large grid' do

        # this is the real test

        let(:n) { 100 }

        it 'agrees with the grid calculation' do
          1024.times do
            xmin = n * rand
            ymin = n * rand
            xmax = xmin + 6 * rand
            ymax = ymin + 6 * rand
            coord = [xmin, ymin, xmax, ymax]
            expect(rtree.search(coord))
              .to match_array grid(n, coord)
          end
        end
      end
    end
  end
end
