describe 'RTree' do
  describe '.json_read' do

    it 'responds' do
      expect(RTree).to respond_to(:json_read)
    end

    describe 'bad argument types' do
      subject { RTree.json_read(file) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'string' do
        let(:file) { 'not-a-file' }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'StringIO object' do
        let(:file) { StringIO.new }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end
    end

    describe 'argument is valid JSON stream' do

      # generally one would use fixtures here, but JSON setialisation
      # is non-portable (across platforms with different page-sizes,
      # for example), so instead we create the JSON stream by reading
      # the output of #json_write, so these are round-trip tests really.

      let(:path) { Tempfile.new('json-read-spec') }
      let(:instance) { RTree.new(2) }

      before do
        nodes.each { |node| instance.add_rect(*node) }
        File.open(path, 'w') { |file| instance.json_write(file) }
      end

      subject do
        File.open(path, 'r') { |file| RTree.json_read(file) }
      end

      context 'empty tree' do
        let(:nodes) { [] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end

      context 'non-empty tree' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end
      end
    end
  end
end
