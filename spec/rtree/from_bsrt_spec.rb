describe 'RTree' do
  describe '.from_bsrt' do

    it 'responds' do
      expect(RTree).to respond_to :from_bsrt
    end

    context 'bad arguments' do
      subject { RTree.from_bsrt(bsrt) }

      describe 'nil' do
        let(:bsrt) { nil }

        it 'raises' do
          expect { subject }.to raise_error TypeError
        end
      end

      describe 'hash' do
        let(:bsrt) { { key: 'value' } }

        it 'raises' do
          expect { subject }.to raise_error TypeError
        end
      end

      describe 'invalid BSRT (short)' do
        let(:bsrt) { 'meh' }

        it 'raises' do
          expect { subject }.to raise_error RuntimeError
        end
      end

      describe 'invalid BSRT (long, with correct BSRt magic' do
        let(:bsrt) { 'BSRt-quack-quack-quack' }

        it 'raises' do
          expect { subject }.to raise_error Errno::EINVAL
        end
      end
    end

    context 'with RTree #to_bsrt output (round trip)' do

      # this relies on RTree #to_bsrt output, so errors here
      # may be a result of an issue in that method

      subject { RTree.from_bsrt(bsrt) }

      let(:instance) { RTree.new(2) }
      let(:bsrt) { instance.to_bsrt }

      before { nodes.each { |node| instance.add_rect(*node) } }

      context 'empty' do
        let(:nodes) { [] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is identical' do
          expect(subject).to eq instance
        end
      end

      context 'non-empty' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is identical' do
          expect(subject).to eq instance
        end
      end
    end
  end
end
