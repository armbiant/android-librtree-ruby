# frozen_string_literal: true

# @!visibility private
#
class RTreeBase ; end

# @!visibility private
#
class RTreeStyleBase ; end

require 'rtree/rtree'
require 'json'
require 'fcntl'

# @author RTree J. J. Green
#
# A Ruby native extension implementing the R-tree spatial index of
# Guttman-Green.  The code is an emebded version of
# {http://soliton.vm.bytemark.co.uk/pub/jjg/en/code/librtree librtree}.
#
# Use
#
#    require 'rtree'
#
# to make the {RTree} class available.
#
# Given a set or rectangles (or higher dimensional cuboids) and their
# associated ids, one can build an {RTree} using the {.csv_read} class
# method or repeated calls to the {#add_rect} instance method.  The
# former is more performant since it avoids routing the rectangles
# through the Ruby-C interface.
#
# Once the {RTree} instance is created one can make spatial queries
# against it with the {#search} method; one passes a rectangle to the
# method and it returns the ids of all of the input rectangles which
# intersect it (or yields them one at a time to a block, if given).
#
# It may be convenient to serialise the {RTree} for faster loading, the
# library implements a custom binary format, BSRT (binary serialised
# R-tree) which can be written by {#bsrt_write} and read with {.bsrt_read}.
# One can also serialise to JSON, but this results in a much larger file
# (a factor of ten) and so correspondingly slow to read/write.  Useful,
# nevertheless, for debugging and loading into a native Ruby hash format
# (see {#to_h}).
#
# All rectangles used in the library are simply arrays of floats, the
# lower value for each dimension, followed by the upper value for each
# dimension.  Thus
#
#    [0, 0, 1, 2]
#
# is the rectangle with 0 ≤ _x_ ≤ 1 and 0 ≤ _y_ ≤ 2. Upper and lower
# values may coincide (to create a line segment in 2-space, for example)
# but a lower value larger than the upper value will cause an error.
#
# It is anticipated that the ids passed to the library will be used as
# an index for application specific data to which this rectangle relates
# (its location in an array, or a DB id) but this is entirely at the
# discretion of the caller, the library makes no use of the value, treating
# it as payload. In particular, the value may be non-unique and may be zero.
# One should note that the id type used internally by the library is
# determined at compile-time (with the RTREE_ID_TYPE variable) and by
# default this is a 64-bit unsigned integer.
#
class RTree < RTreeBase

  # @!visibility private
  #
  module IOUtil
    extend self

    # @!visibility private
    #
    def deserialise(string, encoding)
      raise TypeError unless string.is_a? String
      rd, wr = IO.pipe(encoding)
      if fork then
        wr.close
        unset_nonblock(rd)
        begin
          result = yield(rd)
        ensure
          rd.close
          Process.wait
        end
      else
        rd.close
        begin
          wr.write(string)
        ensure
          wr.close
          exit!
        end
      end
      result
    end

    # @!visibility private
    #
    def serialise(encoding)
      rd, wr = IO.pipe(encoding)
      if fork then
        wr.close
        unset_nonblock(rd)
        begin
          result = rd.read || raise(IOError, 'reading from pipe')
        ensure
          rd.close
          Process.wait
        end
      else
        rd.close
        begin
          yield(wr)
        ensure
          wr.close
          exit!
        end
      end
      result
    end

    private

    # In Ruby 3, the Fibres facility exploits non-blocking IO and so pipes
    # are non-blocking by default, this buggers-up the deserialise method
    # since there is short delay forking here -- so we reinstate blocking
    # with some fairly low-level fcntl(2) magic.

    def unset_nonblock(fd)
      fd.fcntl(
        Fcntl::F_SETFL,
        fd.fcntl(Fcntl::F_GETFL, 0) & ~Fcntl::O_NONBLOCK
      )
    end

  end

  class << self

    # Create a new RTree instance from JSON stream
    # @param io [IO] a readable stream object
    # @return [RTree] the newly instantiated RTree
    # @see #json_write
    # @example Read from file
    #   rtree = File.open('rtree.json', 'r') { |io| RTree.json_read(io) }
    #
    def json_read(io)
      super
    end

    # Create a new RTree instance from JSON string
    # @param json [String] a JSON string
    # @return [RTree] the newly instantiated RTree
    # @see #to_json
    #
    def from_json(json)
      deserialise(json, Encoding::UTF_8) { |io| json_read(io) }
    end

    # Create a new RTree instance from BSRT (binary serialised R-tree)
    # stream
    # @param io [IO] a readable stream object
    # @return [RTree] the newly instantiated RTree
    # @see #bsrt_write
    # @example Read from file
    #   rtree = File.open('rtree.bsrt', 'r') { |io| RTree.bsrt_read(io) }
    #
    def bsrt_read(io)
      super
    end

    # Create a new RTree instance from BSRT (binary serialised R-tree)
    # string
    # @param bsrt [String] a binary encoded string
    # @return [RTree] the newly instantiated RTree
    # @see #to_bsrt
    #
    def from_bsrt(bsrt)
      deserialise(bsrt, Encoding::BINARY) { |io| bsrt_read(io) }
    end

    # Build a new RTree instance from CSV stream
    # @param io [IO] a readable stream object
    # @param dim [Integer] the dimension of the tree
    # @param split [:linear, :quadratic, :greene] See {#initialize}
    # @param node_page [Integer] See {#initialize}
    # @return [RTree] the newly built RTree
    # @note The CSV file (without header) should have the id in the
    #   first column, then twice as many floats as the dimension.
    #   Extra columns may be present and will be ignored (this
    #   useful feature is the reason that the dimension is a required
    #   argument).
    #
    def csv_read(io, dim, split: :quadratic, node_page: 0)
      flags = split_flag(split) | node_page_flag(node_page)
      super(io, dim, flags)
    end

    # Build a new RTree instance from CSV string
    # @param csv [String] the CSV data in a string
    # @param dim [Integer] the dimension of the tree
    # @param kwarg [Hash] As for {.csv_read}
    # @return [RTree] the newly built RTree
    #
    def from_csv(csv, dim, **kwarg)
      deserialise(csv, Encoding::UTF_8) do |io|
        csv_read(io, dim, **kwarg)
      end
    end

    # @return [Array<Integer>] version of librtree
    #
    def version
      @version ||= super.split('.').map(&:to_i)
    end

    # @return [String] email address for librtree bug reports
    #
    def bugreport
      super
    end

    # @return [String] librtree homepage
    #
    def url
      super
    end

    # @!visibility private
    # This seems to need the self qualification on the constants,
    # without them we get a NameError, not sure why so asked on SO
    # https://stackoverflow.com/questions/68122256/ It's still not
    # clear to me why this qualification is needed, but that's a
    # problem with my understanding of the Ruby Eigenclass, it is
    # the expected behaviour.  Note that self.const_get(:FOO) is
    # the same as self::FOO, but the latter gives a spurious
    # type-warning from steep
    #
    def split_flag(split)
      self.const_get(split_symbol(split))
    end

    # @!visibility private
    #
    def node_page_flag(node_page)
      node_page << 2
    end

    private

    def deserialise(string, encoding, &block)
      RTree::IOUtil.deserialise(string, encoding, &block)
    end

    def split_symbol(split)
      case split
      when :quadratic
        :SPLIT_QUADRATIC
      when :linear
        :SPLIT_LINEAR
      when :greene
        :SPLIT_GREENE
      else
        raise ArgumentError, "bad split value: #{split}"
      end
    end
  end

  # Initialize a new (empty) RTree
  # @param dim [Integer] the dimension of the tree
  # @param split [:linear, :quadratic, :greene] determines the splitting
  #   strategy, the linear strategy is faster to build, the quadratic
  #   and greene strategies produce better-quality R-trees which are
  #   faster to query.
  # @param node_page [Integer] the nodes-per-page value. This value can
  #   affect performance quite dramatically, particularly build time. A
  #   value which is too large would result in an infeasible branching
  #   factor for the R-tree and will cause the function to error with errno
  #   set to EINVAL. A value of zero is permitted and the default; in
  #   this case the function will choose a good value based on heuristics.
  #   You may get better performance for your use-case by manual
  #   experimentation, but zero is a good place to start.
  # @return [RTree] the newly instantiated RTree
  #
  def initialize(dim, split: :quadratic, node_page: 0)
    @split = split
    @node_page = node_page
    super(dim, flags)
  end

  # Create a deep copy
  # @return [RTree] a deep copy of the RTree
  #
  def clone
    super
  end

  # Add a rectangle to the RTree
  # @param id [Integer] the id of the rectangle. It is anticipated that
  #   the id will be used as an index for application specific data to
  #   which this rectangle relates, but this is entirely at the discretion
  #   of the caller, the library makes no use of the value, treating it as
  #   payload. In particular, the value may be non-unique and may be zero.
  # @param coords [Array<Float>] the extent of the rectangle, the minima
  #   for each dimension, then the maxima for each dimension.
  # @return [self]
  # @example Add a rectangle to a dimension two RTree
  #   rtree = RTree.new(2)
  #   rtree.add_rect(7, [0, 0, 1, 1])
  #
  def add_rect(id, coords)
    super
  end

  # Search the RTree
  # @param coords [Array<Float>] the search rectangle, as {#add_rect}
  # @return [Array<Integer>] the ids of all rectangles which intersect
  #   the search rectangle. If a block is given then these values will
  #   be yielded to the block (one at a time).
  #
  def search(coords)
    if block_given? then
      super
    else
      ids = []
      super(coords) { |id| ids << id }
      ids
    end
  end

  # Update the RTree.  Modifies the rectangles in-place without changing
  # the tree structure.  Provided that the changes are small, the search
  # efficiency should be close to that of freshly built RTree.
  # @yieldparam id [Integer] see {#add_rect}
  # @yieldparam coords [Array<Float>] see {#add_rect}
  # @yieldreturn [Array<Float>] the modified rectangle extent
  # @return [self]
  # @example Shift all rectangles by ε
  #   rtree.update! { |id, coords| coords.map { |x| x + ε } }
  # @note In the C library, the implementation (via a C callback function)
  #   is much faster than rebuilding the R-tree; in this Ruby interface
  #   the callback must convert C floats to Ruby, yield them to the
  #   block and then convert the returned Ruby Floats to C; so we would
  #   expect that it loses much of its competitive advantage when
  #   compared to an R-tree rebuild.
  #
  def update!
    super
  end

  # The height to the tree in the usual mathematical sense
  # @return [Integer] the tree height
  #
  def height
    super
  end

  # Whether the RTree has any rectangles or not
  # @return [Boolean] true if the RTree is empty
  #
  def empty?
    super
  end

  # Serialise to JSON stream
  # @param io [IO] a writable stream
  # @return [self]
  # @see .json_read
  # @example Write to file
  #   File.open('rtree.json', 'w') { |io| rtree.json_write(io) }
  #
  def json_write(io)
    super
  end

  # Serialise to BSRT (binary serialised R-tree) stream
  # @param io [IO] a writable stream
  # @return [self]
  # @see .bsrt_read
  # @example Write to file
  #   File.open('rtree.bsrt', 'w') { |io| rtree.bsrt_write(io) }
  #
  def bsrt_write(io)
    super
  end

  # Serialise to JSON string
  # @return [String] the UTF-8 encoded JSON
  # @see .from_json
  #
  def to_json
    serialise(Encoding::UTF_8) { |io| json_write(io) }
  end

  # Serialise to BSRT string
  # @return [String] the binary encoded BSRT
  # @see .from_bsrt
  #
  def to_bsrt
    serialise(Encoding::BINARY) { |io| bsrt_write(io) }
  end

  # The RTree structure in hash form
  # @return [Hash]
  #
  def to_h
    JSON.parse(to_json, symbolize_names: true)
  end

  # Equality of RTrees.  This is a rather strict equality,
  # not only must the tree have the same rectangles, they
  # must be in the same order. Certainly {#clone} will produce
  # an instance which is equal in this sense.
  # @return [Boolean] true if the instances are identical
  #
  def eq?(other)
    super
  end

  alias_method(:==, :eq?)

  # @return [Integer] the dimension of the R-tree
  #
  def dim
    super
  end

  # @return [Integer] the total bytes allocated for the instance
  # @note This method traverses the entire tree summing the
  #   contributions for each node (rather than maintaining a
  #   running count).  Performance-minded users may wish to
  #   cache this value (invalidating the cache when calling
  #   {#add_rect} of course).
  #
  def size
    super
  end

  # @return [Integer] the bytes in a page of memory
  #
  def page_size
    super
  end

  # @return [Integer] the size in bytes of a node
  #
  def node_size
    super
  end

  # @return [Integer] the size in bytes of a rectangle
  #
  def rect_size
    super
  end

  # @return [Integer] the size in bytes of a branch
  #
  def branch_size
    super
  end

  # @return [Integer] the number of branches from each node
  #
  def branching_factor
    super
  end

  # @return [Float] the volume of the unit sphere in the R-tree's
  #   dimension
  #
  def unit_sphere_volume
    super
  end

  # Create a PostScript plot of the RTree
  #
  # @param io [IO] a writeable stream object
  # @param style [RTree::Style] a style object describing the fill
  #   colour and stroke width and colour for each level of the tree.
  # @param height [Float] the height of the plot in units of PostScript
  #   point (1/72 inch)
  # @param width [Float] the width of the plot in units of PostScript
  #   point (1/72 inch), if neither height nor width is given then a
  #   width of 216 (3 inches) will be taken as default
  # @param margin [Float] extra space around the plot in units of
  #   PostScript point (1/72 inch), default zero
  #
  def postscript(io, style, height: nil, width: nil, margin: 0)
    if height && width then
      raise ArgumentError, 'cannot specify both height and width'
    end
    if height then
      axis = AXIS_HEIGHT
      extent = height
    else
      axis = AXIS_WIDTH
      extent = width || 216
    end
    super(style, axis, extent, margin, io)
  end

  private

  attr_reader :split, :node_page

  def split_flag
    self.class.split_flag(split)
  end

  def node_page_flag
    self.class.node_page_flag(node_page)
  end

  def flags
    node_page_flag | split_flag
  end

  def serialise(encoding, &block)
    RTree::IOUtil.serialise(encoding, &block)
  end

end

# @author RTree::Style J. J. Green
#
# A Ruby wrapper around RTree styles, used in PostScript plotting.
# in particular by {RTree#postscript}.
#
class RTree::Style < RTreeStyleBase

  class << self

    # Create a new Style instance from JSON stream
    # @param io [IO] a readable stream object
    # @return [RTree::Style] the newly instantiated Style
    # @example Read from file
    #   style = File.open('some.style', 'r') do |io|
    #     RTree::Style.json_read(io)
    #   end
    #
    def json_read(io)
      super
    end

    # Create a new Style instance from JSON string
    # @param json [String] a JSON string
    # @return [RTree::Style] the newly instantiated Style
    # @see .json_read
    #
    def from_json(json)
      deserialise(json, Encoding::UTF_8) { |io| json_read(io) }
    end

    # Create a new Style instance from array of Hash
    # @param array [Array] an array of hash, this will be converted
    #   to JSON and read by .from_json
    # @return [RTree::Style] the newly instantiated Style
    # @see .from_json
    # @example Create a simple one-level style:
    #   style = RTree::Style.from_a(
    #     [
    #       {
    #         fill: {
    #           rgb: [0.5, 0.5, 0.5]
    #         },
    #         stroke: {
    #           rgb: [0, 0, 1],
    #           width: 3
    #         }
    #       }
    #     ]
    #   )
    #
    def from_a(array)
      from_json(array.to_json)
    end

    alias_method :from_array, :from_a

    private

    def deserialise(string, encoding, &block)
      RTree::IOUtil.deserialise(string, encoding, &block)
    end

  end
end
