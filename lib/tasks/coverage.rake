namespace :coverage do
  COV_DIR = 'tmp/coverage'

  desc 'Open coverage in browser'
  task :show do
    sh('xdg-open', "#{COV_DIR}/index.html")
  end

  desc 'Clean coverage artefacts'
  task :clean do
    Rake::Cleaner.cleanup_files(FileList["#{COV_DIR}/**/*"])
  end
end

Rake::Task['clean'].enhance ['coverage:clean']
