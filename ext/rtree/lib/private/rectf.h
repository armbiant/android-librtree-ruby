/*
  private/rectf.h
  Copyright (c) J.J. Green 2022
*/

#ifndef PRIVATE_RECTF_H
#define PRIVATE_RECTF_H

#include <rtree/types.h>
#include <stddef.h>

typedef rtree_coord_t (rectf_rsv_t)(size_t, const rtree_coord_t*);

rectf_rsv_t* rectf_spherical_volume(size_t);

typedef void (rectf_rc_t)(size_t,
                          const rtree_coord_t*,
                          const rtree_coord_t*,
                          rtree_coord_t*);

rectf_rc_t* rectf_combine(size_t);

#endif
