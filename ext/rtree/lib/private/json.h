/*
  private/json.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PRIVATE_JSON_H
#define PRIVATE_JSON_H

#include <rtree.h>

#include <stdio.h>

int json_rtree_write(const rtree_t*, FILE*);
rtree_t* json_rtree_read(FILE*);

#endif
