/*
  private/postscript.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PRIVATE_POSTSCRIPT_H
#define PRIVATE_POSTSCRIPT_H

#include <private/node.h>
#include <private/state.h>

#include <rtree/postscript.h>

int postscript_write(const state_t*, const node_t*,
                     const rtree_postscript_t*, FILE*);

#endif
