/*
  private/page.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PRIVATE_PAGE_H
#define PRIVATE_PAGE_H

#include <stddef.h>

int page_size(size_t*);

#endif
