/*
  rtree/node.h
  Copyright (c) J.J. Green 2019
*/

#ifndef RTREE_NODE_H
#define RTREE_NODE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <rtree/types.h>

typedef int (rtree_update_t)(rtree_id_t, rtree_coord_t*, void*);

#ifdef __cplusplus
}
#endif

#endif
