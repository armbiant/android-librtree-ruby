/*
  rtree/extent.h
  Just an enum indicating a choice of axis (direction)
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_EXTENT_H
#define RTREE_EXTENT_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum { axis_width, axis_height } extent_axis_t;

#ifdef __cplusplus
}
#endif

#endif
