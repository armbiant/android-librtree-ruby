require 'mkmf'

$VPATH << "$(srcdir)/lib"
$srcs =
  Dir.glob("#{$srcdir}/{,lib/}*.c")
    .map { |path| File.basename(path) }
    .sort

append_cppflags('-I$(srcdir)/lib')
append_cppflags('-DHAVE_CONFIG_H')

check_sizeof('rtree_coord_t', 'rtree/types.h')
check_sizeof('rtree_id_t', 'rtree/types.h')

if have_header('endian.h') ||
   have_header('sys/endian.h') ||
   have_header('libkern/OSByteOrder.h') ||
   have_header('winsock2.h')
then
  append_cflags('-DWITH_BSRT')
else
  warn('cannot determine endianness, no BSRT support')
end

if have_header('jansson.h') &&
   have_library('jansson', 'json_pack')
then
  append_cflags('-DWITH_JSON')
else
  warn('cannot find jansson, no JSON support')
end

unless have_header('unistd.h') &&
       (
         have_func('sysconf', 'unistd.h') ||
         have_func('getpagesize', 'unistd.h')
       )
then
  warn('cannot find page-size functions, will guess')
end

have_header('features.h')

if have_macro('__has_builtin') then
  src = <<~EOF
    #if __has_builtin(__builtin_ctzl)
    #else
    #error no __builtin_ctzl
    #endif
  EOF
  checking_for(checking_message('__builtin_ctzl')) do
    if result = try_compile(src) then
      $defs.push('-DHAVE___BUILTIN_CTZL')
      result
    end
  end
end

create_header('config.h')
create_makefile('rtree')
